-- Answers to exercise 2 questions


-- A

SELECT s.fname, s.lname, a.dept, a.num
FROM unidb_attend AS a , unidb_students AS s
WHERE a.id = s.id AND a.dept = 'comp' AND a.num = '219';

-- B

SELECT s.fname, s.lname, c.dept, c.num, c.rep_id, s.country
FROM unidb_courses AS c , unidb_students AS s
WHERE c.rep_id = s.id AND NOT s.country ='NZ';


-- C

SELECT DISTINCT l.office
FROM unidb_lecturers AS l , unidb_teach AS t
WHERE l.staff_no = t.staff_no AND t.dept = 'comp' AND t.num = '219';

-- D

SELECT s.fname, s.lname, l.fname, l.lname
FROM unidb_students AS s, unidb_teach AS t, unidb_attend AS a , unidb_lecturers AS l
WHERE s.id = a.id AND t.staff_no = l.staff_no AND a.dept = t.dept AND  a.num=t.num AND l.fname = 'Te Taka';


-- E?

SELECT
    CONCAT(student.fname, ' ', student.lname) AS student_name,
    CONCAT(mentor.fname, ' ', mentor.lname) AS mentor_name
FROM
    unidb_students AS student,
    unidb_students AS mentor
WHERE student.mentor = mentor.id;



-- F ??

SELECT l.lname, l.fname
    FROM unidb_lecturers AS l
    WHERE l.office LIKE 'G%'
UNION
SELECT s.lname, s.fname
    FROM unidb_students AS s
    WHERE s.country NOT LIKE 'NZ';



-- G

SELECT c.dept, c.num, c.rep_id, s.id, s.fname, s.lname , c.coord_no, l.staff_no, l.fname, l.lname
FROM unidb_courses AS c, unidb_students AS s, unidb_lecturers AS l
WHERE c.dept = 'comp' AND c.num = '219' AND c.rep_id = s.id AND c.coord_no = l.staff_no;
