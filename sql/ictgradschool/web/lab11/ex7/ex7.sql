
DROP TABLE IF EXISTS db_ex7_Team;
DROP TABLE IF EXISTS db_ex7_Player;

CREATE TABLE IF NOT EXISTS db_ex7_Team(
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(64),
    city VARCHAR(64),
    captain VARCHAR(64),
    points INT,

    PRIMARY KEY(id)

);

INSERT INTO db_ex7_Team(name, city, captain, points) VALUES
    ('Rovers', 'Dunedin', 'John Grantham', 56),
    ('Buccaneers', 'Pukekohe', 'William Scott', 65),
    ('Bulldogs', 'Gisborne', 'Brett Thompson', 47);


CREATE TABLE IF NOT EXISTS db_ex7_Player(
    id INT NOT NULL AUTO_INCREMENT,
    nameTeam VARCHAR(64),
    namePlayer VARCHAR(64),
    age INT,
    country VARCHAR(64),


    PRIMARY KEY(id)
);


FOREIGN KEY(nameTeam) REFERENCES db_ex7_Team(id),


INSERT INTO db_ex7_Player(nameTeam, namePlayer, age, country) VALUES
    ('Rovers', 'Hayden', 26, 'New Zealand'),
    ('Buccaneers', 'Andrew', 32, 'Australia'),
    ('Bulldogs', 'James', 35, 'New Zealand');

SELECT * FROM db_ex7_Team;
SELECT * FROM db_ex7_Player;

-- Need to add foreign key after creation of both tables, using the alter table function.
