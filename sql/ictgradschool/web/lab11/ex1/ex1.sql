-- Answers to exercise 1 questions
-- A
SELECT DISTINCT dept FROM unidb_courses;

-- B
SELECT DISTINCT semester FROM unidb_attend;

-- C
SELECT DISTINCT dept, num FROM unidb_attend;

-- D
SELECT fname, lname country FROM unidb_students
ORDER BY fname ASC;

-- E
SELECT fname,lname, mentor FROM unidb_students
ORDER BY mentor;

-- F
SELECT fname, lname, office FROM unidb_lecturers
ORDER BY office;

-- G
SELECT fname, lname, staff_no FROM unidb_lecturers
WHERE staff_no >500;


-- H
SELECT fname, lname, id FROM unidb_students
WHERE id >1668 AND id< 1824;

-- I
SELECT fname, lname, country FROM unidb_students
WHERE country = 'US' OR country = 'NZ' OR country='AU';

-- J

SELECT fname, lname, office
FROM unidb_lecturers
WHERE office LIKE 'G%';


-- K
SELECT DISTINCT dept, num FROM unidb_courses
WHERE NOT dept= 'comp';


-- L
SELECT fname, lname, country FROM unidb_students
WHERE country = 'FR' OR country = 'MX';


